from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"

    headers = {
        "Authorization": PEXELS_API_KEY
    }
    response = requests.get(url, headers=headers)
    data = response.json()

    return data["photos"][0]["src"]["original"]


def get_coord(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},&limit=&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)
    return {"lat": response.json()[0]["lat"], "lon": response.json()[0]["lon"]}


def get_weather_data(lat, lon):
    url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"

    response = requests.get(url)

    return {
        "description": response.json()["weather"][0]["description"],
        "temp": response.json()["main"]["temp"],
    }
